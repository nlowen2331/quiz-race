import { useSelector, useDispatch} from 'react-redux';
import './css/App.css';
import { changeScreen } from './redux/actions/changeScreen';
import CreateRoom from './screens/CreateRoom';
import Results from './screens/Results'
import JoinRoom from './screens/JoinRoom';
import Landing from './screens/Landing';

function App() {

  const displayScreen = useSelector(state=>state.displayScreen)
  const dispatch = useDispatch()

  let CurrentScreen
  switch (displayScreen){
      case 'JOIN_ROOM':
        CurrentScreen=<JoinRoom/>
        break;
      case 'CREATE_ROOM':
        CurrentScreen=<CreateRoom/>
        break;
      case 'RESULTS':
        CurrentScreen=<Results/>
        break;
      default:
        CurrentScreen=<Landing/>
  }

  return (
    <>
      {CurrentScreen}
    </>
  );
}

export default App;
