
export const displayScreen = (state='LANDING',action) =>{
    switch(action.type){
        case 'CHANGE_SCREEN':
            return action.screen
        default:
            return state
    }
}