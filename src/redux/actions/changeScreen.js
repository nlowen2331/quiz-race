
export const changeScreen = (screen) =>{
    return (
        {
            type: 'CHANGE_SCREEN',
            screen
        }
    )
}