export const questionTypes =
     [
        {label: 'Geography', value: 'geo'},
        {label: 'Space', value: 'spa'},
        {label: 'Math', value: 'mat'},
        {label: 'Movies', value: 'mov'},
    ]

export const questionAmounts =
     [
        {label: '10', value: 10},
        {label: '15', value: 15},
        {label: '20', value: 20},
        {label: '25', value: 25},
        {label: '30', value: 30},
    ]

export const questionTimes = 
    [
        {label: '10s', value: 10},
        {label: '20s', value: 20},
        {label: '30s', value: 30},
        {label: '40s', value: 40},
        {label: '50s', value: 50},
        {label: '60s', value: 60},
        {label: '70s', value: 70},
        {label: '80s', value: 80},
    ]
