import React from 'react'
import Select from 'react-select'
import '../css/CreateRoom.css'
import { questionAmounts, questionTimes, questionTypes } from '../data/CreateOptions'
import {BiArrowBack} from 'react-icons/bi'
import { changeScreen } from '../redux/actions/changeScreen'
import { useDispatch } from 'react-redux'

function CreateRoom() {

    const dispatch = useDispatch()

    return (
        <div className="create-room">
            <BiArrowBack className='icon' onClick={()=>dispatch(changeScreen('LANDING'))}/>
            <h1>Create Room</h1>
            <div className='input-container'>
                <div>
                    <h3>Question Type(s)</h3>
                    <Select options={questionTypes}/>
                </div>
                <div>
                    <h3>Number of Questions</h3>
                    <Select options={questionAmounts}/>
                </div>
                <div>
                    <h3>Time Length</h3>
                    <Select options={questionTimes}/>
                </div>
            </div>
            <div className='btn-container'>
                <button>Create!</button>
            </div>
        </div>
    )
}

export default CreateRoom
