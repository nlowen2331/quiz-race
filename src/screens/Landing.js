import React from 'react'
import { useDispatch } from 'react-redux'
import '../css/Landing.css'
import { changeScreen } from '../redux/actions/changeScreen'

function Landing({...props}) {

    const dispatch = useDispatch()

    return (
        <div className='landing'>
            <h1>Welcome To Quiz Race!</h1>
            <div className='btn-container'>
                <button onClick={()=>dispatch(changeScreen('CREATE_ROOM'))}>Create Room</button>
                <button onClick={()=>dispatch(changeScreen('JOIN_ROOM'))}>Join Room</button>
            </div>  
        </div>
    )
}

export default Landing
