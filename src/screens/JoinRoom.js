import React from 'react'
import {BiArrowBack} from 'react-icons/bi'
import { useDispatch } from 'react-redux'
import '../css/JoinRoom.css'
import { changeScreen } from '../redux/actions/changeScreen'

function JoinRoom() {

    const dispatch = useDispatch()

    return (
        <div className='join-room'>
            <BiArrowBack className='icon' onClick={()=>dispatch(changeScreen('LANDING'))}/>
            <h1>Enter Room Code</h1>
            <input/>
            <div className='button-container'>
                <button>Join</button>
            </div>
        </div>
    )
}

export default JoinRoom
